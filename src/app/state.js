import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    city: null
  },
  mutations: {
    setCity (state, newCity) {
      state.city = newCity
      this.$cookies.set('city', newCity)
    },
    init (state) {
      if (this.$cookies.get('city')) {
        state.city = this.$cookies.get('city')
      }
    }
  }
})