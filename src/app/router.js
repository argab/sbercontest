import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'AllPartners',
        component: () => import('./partners/all_partners.vue'),
        props: route => ({ searchString: route.query.search ? route.query.search : '' })
    },
    {
        path: '/partner/:id',
        name: 'Partner',
        component: () => import('./partners/partner.vue')
    },
    {
        path: '/not-found',
        name: 'NotFound',
        component: () => import('./errors/not_found.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    routes,
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition)
          return savedPosition
        return { x: 0, y: 0 }
    }
})

export default router