export default {
    "status": 200,
    "data": {
        "id": "43",
        "name": "До 15% бонусами в категории «Любимый БУРГЕР КИНГ»",
        "cityCode": "Samara",
        "section": [
            {
                "id": "5",
                "name": "Здоровье"
            },
            {
                "id": "6",
                "name": "Кафе и рестораны"
            }
        ],
        "type": "federal",
        "image": "http://spasibox2.baccasoft.ru/upload/iblock/95f/95fa177a508336d268cbb02e42940340.jpg",
        "dateFrom": 1617279000,
        "dateTo": 1619784600,
        "typeMarkup": "COMMON",
        "detailText": "<p>\r\n\tЗаходите в БУРГЕР КИНГ. Здесь вас ждет множество новинок – Воппер Барбекю, Лютый Воппер, Чикен Барбекю, Чикен Ролл Барбекю, салат Коул Слоу, ну и, конечно же, бессмертная классика – вопперы, бургеры с разными начинками, десерты, милкшейки и многое другое.\r\n</p>\r\n<p>\r\n\tЗдесь выбор найдет каждый, как и повышенные бонусы!\r\n</p>\r\n<p>\r\n\tДо 5 марта 2020 года получайте до 21% СПАСИБО за покупки по карте Сбербанка на сумму от 400 рублей в БУРГЕР КИНГ:\r\n</p>\r\n<div class=\"sales-offer__block\">\r\n\t<div class=\"sales-offer__row\">\r\n <span class=\"sales-offer__percent\">21%</span>\r\n\t\t<div class=\"sales-offer__text\">\r\n\t\t\tбонусов при оплате Молодежными картами Сбербанка;\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"sales-offer__row\">\r\n <span class=\"sales-offer__percent\">16%</span>\r\n\t\t<div class=\"sales-offer__text\">\r\n\t\t\tбонусов при оплате другими картами Сбербанка.\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<p>\r\n\tПредложение действует в сети ресторанов и в мобильном приложении БУРГЕР КИНГ.\r\n</p>",
        "percents": [
            {
                "value": "21%",
                "description": "бонусов при оплате Молодежными картами Сбербанка;"
            },
            {
                "value": "16%",
                "description": "бонусов при оплате другими картами Сбербанка."
            }
        ],
        "percents_location": "after_description",
        "files": [
            {
                "name": "Правила акции",
                "path": "http://spasibox2.baccasoft.ru/upload/iblock/71e/71e565e619b55dd66e81ec693a006fe4.pdf"
            }
        ],
        "partner": {
            "id": "44",
            "name": "Бургер Кинг",
            "logo": "http://spasibox2.baccasoft.ru/upload/iblock/aca/aca17a119a23a2609570930dd60b32c7.png",
            "site": "www.burgerking.ru ",
            "description": "Сеть ресторанов БУРГЕР КИНГ известна всем вкусом фирменных блюд и отличным соотношением цены и качества. Основанный в 1954 году, БУРГЕР КИНГ занимает второе по величине место в мире среди сетей ресторанов быстрого питания, специализирующихся на гамбургерах.<br>\r\n <br>\r\n Оригинальный бургер «Воппер», ингредиенты высшего качества, фирменные рецепты и комфорт для посещения всей семьей — вот то, что уже пятьдесят с лишним лет является отличительной чертой бренда БУРГЕР КИНГ.<br>\r\n <br>",
            "displayMap": true,
            "partnerLocation": [
                {
                    "address": "Россия, Свердловская область, Екатеринбург, улица Академика Шварца, 15",
                    "workingHours": [],
                    "phoneNumber": [
                        "+7 (495) 544-50-00"
                    ],
                    "coordinates": {
                        "lat": "56.797038",
                        "lon": "60.622735"
                    }
                },
                {
                    "address": "ул. 22-го Партсъезда, 2, Орджоникидзевский район,",
                    "workingHours": [],
                    "phoneNumber": [],
                    "coordinates": {
                        "lat": "56.886115",
                        "lon": "60.583299"
                    }
                },
                {
                    "address": "Россия, Свердловская область, Екатеринбург, улица Айвазовского, 53",
                    "workingHours": [],
                    "phoneNumber": [],
                    "coordinates": {
                        "lat": "56.802272",
                        "lon": "60.59875"
                    }
                },
                {
                    "address": "Россия, Свердловская область, Екатеринбург, улица Горького, 36",
                    "workingHours": [],
                    "phoneNumber": [],
                    "coordinates": {
                        "lat": "56.829266",
                        "lon": "60.607248"
                    }
                },
                {
                    "address": "Адрес 2",
                    "workingHours": [
                        {
                            "DAY": "пн-пт",
                            "TIME": "10-19"
                        }
                    ],
                    "phoneNumber": [
                        "7777777",
                        "7777777",
                        "7777777",
                        "7777777"
                    ],
                    "coordinates": {
                        "lat": "56.859665",
                        "lon": "60.61978"
                    }
                }
            ]
        }
    }
}