export default {
  "status": 200,
  "data": [
    {
      "id": "43",
      "name": "До 15% бонусами в категории «Любимый БУРГЕР КИНГ»",
      "cityCode": "Samara",
      "section": [
          {
              "id": "5",
              "name": "Здоровье"
          },
          {
              "id": "6",
              "name": "Кафе и рестораны"
          }
      ],
      "type": "federal",
      "image": "/img/bk_promo.jpeg",
      "dateFrom": 1617279000,
      "dateTo": 1619784600,
      "typeMarkup": "COMMON",
      "detailText": "<p>\r\n\tЗаходите в БУРГЕР КИНГ. Здесь вас ждет множество новинок – Воппер Барбекю, Лютый Воппер, Чикен Барбекю, Чикен Ролл Барбекю, салат Коул Слоу, ну и, конечно же, бессмертная классика – вопперы, бургеры с разными начинками, десерты, милкшейки и многое другое.\r\n</p>\r\n<p>\r\n\tЗдесь выбор найдет каждый, как и повышенные бонусы!\r\n</p>\r\n<p>\r\n\tДо 5 марта 2020 года получайте до 21% СПАСИБО за покупки по карте Сбербанка на сумму от 400 рублей в БУРГЕР КИНГ:\r\n</p>\r\n<div class=\"sales-offer__block\">\r\n\t<div class=\"sales-offer__row\">\r\n <span class=\"sales-offer__percent\">21%</span>\r\n\t\t<div class=\"sales-offer__text\">\r\n\t\t\tбонусов при оплате Молодежными картами Сбербанка;\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"sales-offer__row\">\r\n <span class=\"sales-offer__percent\">16%</span>\r\n\t\t<div class=\"sales-offer__text\">\r\n\t\t\tбонусов при оплате другими картами Сбербанка.\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<p>\r\n\tПредложение действует в сети ресторанов и в мобильном приложении БУРГЕР КИНГ.\r\n</p>",
      "percents": [
          {
              "value": "21%",
              "description": "бонусов при оплате Молодежными картами Сбербанка;"
          },
          {
              "value": "16%",
              "description": "бонусов при оплате другими картами Сбербанка."
          }
      ],
      "percents_location": "after_description",
      "files": [
          {
              "name": "Правила акции",
              "path": "http://spasibox2.baccasoft.ru/upload/iblock/71e/71e565e619b55dd66e81ec693a006fe4.pdf"
          }
      ],
      "partner": {
          "id": "43",
          "name": "Бургер Кинг",
          "logo": "/img/bk_logo.png",
          "site": "www.burgerking.ru ",
          "description": "Сеть ресторанов БУРГЕР КИНГ известна всем вкусом фирменных блюд и отличным соотношением цены и качества. Основанный в 1954 году, БУРГЕР КИНГ занимает второе по величине место в мире среди сетей ресторанов быстрого питания, специализирующихся на гамбургерах.<br>\r\n <br>\r\n Оригинальный бургер «Воппер», ингредиенты высшего качества, фирменные рецепты и комфорт для посещения всей семьей — вот то, что уже пятьдесят с лишним лет является отличительной чертой бренда БУРГЕР КИНГ.<br>\r\n <br>",
          "displayMap": true,
          "partnerLocation": [
              {
                  "address": "Россия, Свердловская область, Екатеринбург, улица Академика Шварца, 15",
                  "workingHours": [],
                  "phoneNumber": [
                      "+7 (495) 544-50-00"
                  ],
                  "coordinates": {
                      "lat": "56.797038",
                      "lon": "60.622735"
                  }
              },
              {
                  "address": "ул. 22-го Партсъезда, 2, Орджоникидзевский район,",
                  "workingHours": [],
                  "phoneNumber": [],
                  "coordinates": {
                      "lat": "56.886115",
                      "lon": "60.583299"
                  }
              },
              {
                  "address": "Россия, Свердловская область, Екатеринбург, улица Айвазовского, 53",
                  "workingHours": [],
                  "phoneNumber": [],
                  "coordinates": {
                      "lat": "56.802272",
                      "lon": "60.59875"
                  }
              },
              {
                  "address": "Россия, Свердловская область, Екатеринбург, улица Горького, 36",
                  "workingHours": [],
                  "phoneNumber": [],
                  "coordinates": {
                      "lat": "56.829266",
                      "lon": "60.607248"
                  }
              },
              {
                  "address": "Адрес 2",
                  "workingHours": [
                      {
                          "DAY": "пн-пт",
                          "TIME": "10-19"
                      }
                  ],
                  "phoneNumber": [
                      "7777777",
                      "7777777",
                      "7777777",
                      "7777777"
                  ],
                  "coordinates": {
                      "lat": "56.859665",
                      "lon": "60.61978"
                  }
              }
          ]
      }
    },
    {
      "id": "45",
      "name": "До 15% бонусами в категории «Любимый БУРГЕР КИНГ»",
      "cityCode": "Samara",
      "section": [
          {
              "id": "5",
              "name": "Здоровье"
          },
          {
              "id": "6",
              "name": "Кафе и рестораны"
          }
      ],
      "type": "federal",
      "image": "/img/bk_promo.jpeg",
      "dateFrom": 1617279000,
      "dateTo": 1619784600,
      "typeMarkup": "COMMON",
      "detailText": "<p>\r\n\tЗаходите в БУРГЕР КИНГ. Здесь вас ждет множество новинок – Воппер Барбекю, Лютый Воппер, Чикен Барбекю, Чикен Ролл Барбекю, салат Коул Слоу, ну и, конечно же, бессмертная классика – вопперы, бургеры с разными начинками, десерты, милкшейки и многое другое.\r\n</p>\r\n<p>\r\n\tЗдесь выбор найдет каждый, как и повышенные бонусы!\r\n</p>\r\n<p>\r\n\tДо 5 марта 2020 года получайте до 21% СПАСИБО за покупки по карте Сбербанка на сумму от 400 рублей в БУРГЕР КИНГ:\r\n</p>\r\n<div class=\"sales-offer__block\">\r\n\t<div class=\"sales-offer__row\">\r\n <span class=\"sales-offer__percent\">21%</span>\r\n\t\t<div class=\"sales-offer__text\">\r\n\t\t\tбонусов при оплате Молодежными картами Сбербанка;\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"sales-offer__row\">\r\n <span class=\"sales-offer__percent\">16%</span>\r\n\t\t<div class=\"sales-offer__text\">\r\n\t\t\tбонусов при оплате другими картами Сбербанка.\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<p>\r\n\tПредложение действует в сети ресторанов и в мобильном приложении БУРГЕР КИНГ.\r\n</p>",
      "percents": [
          {
              "value": "21%",
              "description": "бонусов при оплате Молодежными картами Сбербанка;"
          },
          {
              "value": "16%",
              "description": "бонусов при оплате другими картами Сбербанка."
          }
      ],
      "percents_location": "after_description",
      "files": [
          {
              "name": "Правила акции",
              "path": "http://spasibox2.baccasoft.ru/upload/iblock/71e/71e565e619b55dd66e81ec693a006fe4.pdf"
          }
      ],
      "partner": {
          "id": "45",
          "name": "Бургер Кинг",
          "logo": "http://spasibox2.baccasoft.ru/upload/iblock/aca/aca17a119a23a2609570930dd60b32c7.png",
          "site": "www.burgerking.ru ",
          "description": "Сеть ресторанов БУРГЕР КИНГ известна всем вкусом фирменных блюд и отличным соотношением цены и качества. Основанный в 1954 году, БУРГЕР КИНГ занимает второе по величине место в мире среди сетей ресторанов быстрого питания, специализирующихся на гамбургерах.<br>\r\n <br>\r\n Оригинальный бургер «Воппер», ингредиенты высшего качества, фирменные рецепты и комфорт для посещения всей семьей — вот то, что уже пятьдесят с лишним лет является отличительной чертой бренда БУРГЕР КИНГ.<br>\r\n <br>",
          "displayMap": true,
          "partnerLocation": [
              {
                  "address": "Россия, Свердловская область, Екатеринбург, улица Академика Шварца, 15",
                  "workingHours": [],
                  "phoneNumber": [
                      "+7 (495) 544-50-00"
                  ],
                  "coordinates": {
                      "lat": "56.797038",
                      "lon": "60.622735"
                  }
              },
              {
                  "address": "ул. 22-го Партсъезда, 2, Орджоникидзевский район,",
                  "workingHours": [],
                  "phoneNumber": [],
                  "coordinates": {
                      "lat": "56.886115",
                      "lon": "60.583299"
                  }
              },
              {
                  "address": "Россия, Свердловская область, Екатеринбург, улица Айвазовского, 53",
                  "workingHours": [],
                  "phoneNumber": [],
                  "coordinates": {
                      "lat": "56.802272",
                      "lon": "60.59875"
                  }
              },
              {
                  "address": "Россия, Свердловская область, Екатеринбург, улица Горького, 36",
                  "workingHours": [],
                  "phoneNumber": [],
                  "coordinates": {
                      "lat": "56.829266",
                      "lon": "60.607248"
                  }
              },
              {
                  "address": "Адрес 2",
                  "workingHours": [
                      {
                          "DAY": "пн-пт",
                          "TIME": "10-19"
                      }
                  ],
                  "phoneNumber": [
                      "7777777",
                      "7777777",
                      "7777777",
                      "7777777"
                  ],
                  "coordinates": {
                      "lat": "56.859665",
                      "lon": "60.61978"
                  }
              }
          ]
      }
    },
    {
      "id": "42",
      "name": "20% бонусов за технику Electrolux",
      "cityCode": "Moskva",
      "section": [
        { "id": "6", "name": "Кафе и рестораны" },
        { "id": "7", "name": "Такси" }
      ],
      "type": "regional",
      "image": "/img/tp_promo.png",
      "dateFrom": 1617622380,
      "dateTo": 1619609580,
      "typeMarkup": "COMMON",
      "detailText": "Внимание! Начисление и списание бонусов не производится в магазинах «Технопарк», расположенных в ТЦ «ТВОЙ ДОМ». С адресами ТЦ можно ознакомиться на сайте <a href=\"https://tvoydom.ru/shops/\">tvoydom.ru/shops/</a>. <br>rn<ul>rnt<li> <b>4% </b><b>бонусами от стоимости покупок при оплате картами </b>Visa Gold, Visa Gold Package, Visa Infinite, Visa Platinum, Visa Platinum Премьер, Gold MasterCard, MasterCard Премьер, Platinum MasterCard, MasterCard Elite Package, MasterCard World, AMEX Black, AMEX Platinum, Visa Signature, «Мир» Gold, «Мир» Premium, «Мир» Premium PLUS; <br>rn </li>rnt<li> <b>2% </b><b>бонусами при оплате другими картами СберБанка </b><br>rn </li>rnt<li>rntБонусы не начисляются при оплате товара у курьера. В период с «01» ноября 2019 г. начиления и списания не производятся в отношении Товаров, реализуемых Компанией, под товарным знаком <del>«BORK».</del> <br>rn </li>rn</ul>rn <br>",
      "percents": [
        {
          "value": "20% ",
          "description": "при покупке любой техники Electrolux"
        }
      ],
      "percents_location": "after_description",
      "files": [
      ],
      "partner": {
        "id": "40",
        "name": "Технопарк",
        "logo": "/img/tp_logo.png",
        "site": "https://www.technopark.ru/ ",
        "description": "Интернет-магазин бытовой техники и электроникиrnЧем занимается группа компаний?rn«ТЕХНОПАРК» — сеть магазинов премиальной бытовой техники и электроники для дома. Уже 28 лет предлагаем покупателям продукцию всемирно известных брендов.rnКакие товары представлены в интернет-магазине?rnАссортимент насчитывает более 5 000 товаров в следующих категориях:rnrn    встраиваемая и крупная бытовая техника для кухни;rn    цифровая, аудио- и видеоэлектроника;rn    климатическое оборудование;rn    домашние электроприборы для красоты, спорта и здоровья;rn    посуда;rn    товары для геймеров;rn    детские товары.rnrnТехнику и приборы можно сразу доукомплектовать всем необходимым — на сайте доступен полный спектр аксессуаров. А услуга «Установка» и дополнительная гарантия «VIP Сервис» помогут эффективнее использовать ваше устройство.rnНужна помощь с выбором товара и оформлением заказа?rnПерсональные консультанты магазина «Технопарк» ответят на возникшие вопросы, помогут с выбором техники и организуют доставку в день заказа.rn ссылка https://www.technopark.ru/ ",
        "displayMap": false,
        "partnerLocation": []
      }
    },
    {
      "id": "37",
      "name": "Акция 2",
      "cityCode": "Moskva",
      "section": null,
      "type": "regional",
      "image": "/img/pic.jpeg",
      "dateFrom": 1617262020,
      "dateTo": 1619767620,
      "typeMarkup": "FREE",
      "show_name": false,
      "max_width": false,
      "html": {
        "web": "<p>html</p>",
        "tablet": "<p>html</p>",
        "mobile": "<style></style><p>html</p>",
        "android": "",
        "ios": ""
      },
      "files": [
        {
          "name": "Правила акции",
          "path": "/img/pic.jpeg"
        }
      ],
      "partner": {
        "id": "35",
        "name": "Партнёр 1",
        "logo": "/img/pic.jpeg",
        "site": "site.ru",
        "description": "Описание Описание",
        "displayMap": true,
        "partnerLocation": [
          {
            "address": "адрес 1",
            "workingHours": [
              { "DAY": "пн-пт", "TIME": "10-19" },
              { "DAY": "сб-вс", "TIME": "9-20" }
            ],
            "phoneNumber": ["7777777", "7777777"],
            "coordinates": {
              "lat": "55.740636537286",
              "lon": "37.565155639648"
            }
          },
          {
            "address": "Адрес 2",
            "workingHours": [{ "DAY": "пн-пт", "TIME": "10-19" }],
            "phoneNumber": ["7777777", "7777777", "7777777", "7777777"],
            "coordinates": {
              "lat": "55.761355080927",
              "lon": "37.726517333984"
            }
          }
        ]
      }
    },
    {
      "id": "36",
      "name": "Акция 1",
      "cityCode": "Abakan",
      "section": [{ "id": "6", "name": "Кафе и рестораны" }],
      "type": "federal",
      "image": "/img/pic.jpeg",
      "dateFrom": null,
      "dateTo": null,
      "typeMarkup": "COMMON",
      "detailText": "Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст Текст ",
      "percents": [{ "value": "15%", "description": "Сопроводительный текст" }],
      "percents_location": "before_description",
      "files": [
        {
          "name": "Наименование файла",
          "path": "/img/pic.jpeg"
        },
        {
          "name": "Правила акции",
          "path": "/img/pic.jpeg"
        }
      ],
      "partner": {
        "id": "35",
        "name": "Партнёр 1",
        "logo": "/img/pic.jpeg",
        "site": "site.ru",
        "description": "Описание Описание",
        "displayMap": true,
        "partnerLocation": [
          {
            "address": "адрес 1",
            "workingHours": [
              { "DAY": "пн-пт", "TIME": "10-19" },
              { "DAY": "сб-вс", "TIME": "9-20" }
            ],
            "phoneNumber": ["7777777", "7777777"],
            "coordinates": {
              "lat": "55.740636537286",
              "lon": "37.565155639648"
            }
          },
          {
            "address": "Адрес 2",
            "workingHours": [{ "DAY": "пн-пт", "TIME": "10-19" }],
            "phoneNumber": ["7777777", "7777777", "7777777", "7777777"],
            "coordinates": {
              "lat": "55.761355080927",
              "lon": "37.726517333984"
            }
          }
        ]
      }
    }
  ],
  "count": 5,
  "totalCount": 5,
  "filter": {
    "IBLOCK_CODE": "stocks",
    "ACTIVE": "Y",
    "0": {
      "LOGIC": "OR",
      "0": { "PROPERTY_CITY": "Moskva" },
      "1": { "PROPERTY_TYPE": "9" }
    }
  }
}
