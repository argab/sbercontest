import Axios from 'axios'
import Https from 'https'

Axios.defaults.baseURL = process.env.VUE_APP_SERVER
Axios.defaults.timeout = 10000
Axios.defaults.httpsAgent = new Https.Agent({
    rejectUnauthorized: false
})

 const createClient = (router) => {
    const client = Axios.create({
        headers: {
            'Content-Type': 'application/json',
            'Accept-Language': 'es-ES,es;q=0.8',
            'Content-Type': 'application/json'
        }
    })

    client.interceptors.response.use(undefined, (err) => {
        if (err.response.status === 404) {
            router.push({ name: 'NotFound' })
        }
        return Promise.reject(err)
    })

    return client
}

export default createClient
