import Vue from 'vue'
import App from './app/App.vue'
import router from './app/router'
import createClient from './axios'
import Header from './app/partners/components/header'
import Footer from './app/partners/components/footer'
import vClickOutside from 'v-click-outside'
import { VueSpinners } from '@saeris/vue-spinners'
import UUID from "vue-uuid"
import store from './app/state.js'
import VueCookies from 'vue-cookies'

Vue.config.productionTip = false

Vue.use(UUID)
Vue.use(VueCookies)
Vue.$cookies.config('7d')
Vue.use(vClickOutside)
Vue.use(VueSpinners)

Vue.component('Header', Header)
Vue.component('Footer', Footer)

Vue.prototype.$api = createClient(router)
Vue.prototype.$mocking = !!(+process.env.VUE_APP_MOCK_MODE)

store.$cookies = Vue.$cookies

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
